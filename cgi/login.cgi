#!/usr/bin/python

# Import the CGI, string, sys modules
import cgi, string, sys, os, re, random
import cgitb; cgitb.enable()  # for troubleshooting
import sqlite3
import session

#import packages for sending email
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

#Get Databasedir
MYLOGIN="nfoy"
DATABASE="/homes/"+MYLOGIN+"/cs390-mylink/MyLink/mylink.db"
IMAGEPATH="/homes/"+MYLOGIN+"/cs390-mylink/MyLink/images"

##############################################################
# Define function to generate login HTML form.
def login_form():
    html="""
<html>

<head>
	<title>Info Form</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
</head>

<body>
	<div class="main">
		<h1>MyLink Social Media</h1>

		<h3>Type User and Password:</h3>

		<div class="login-container">
			<FORM METHOD=post ACTION="login.cgi">
				<div class="email">
					<div class="input-label">Email:</div>
					<INPUT TYPE=text NAME="username" />
				</div>
				<div class="password">
					<div class="input-label">Password:</div>
					<INPUT TYPE=text NAME="password" />
				</div>

				<INPUT TYPE=hidden NAME="action" VALUE="login" />
				<INPUT TYPE=submit VALUE="Enter" />
			</FORM>
		</div>

		<div class="signup-container">
			<H1> Don't have an account? Sign up now! </H1>
			<FORM METHOD=post ACTION="login.cgi">
				<div class="email">
					<div class="input-label">Email:</div>
					<INPUT TYPE=text NAME="email" />
				</div>
				<div class="username">
					<div class="input-label">Name:</div>
					<INPUT TYPE=text NAME="username" />
				</div>
				<div class="password">
					<div class="input-label">Password:</div>
					<INPUT TYPE=password NAME="password" />
				</div>
				<INPUT TYPE=hidden NAME="action" VALUE="register" />
				<INPUT TYPE=submit VALUE="Enter" />
			</FORM>
		</div>
	</div>
</body>

</html>
"""
    print_html_content_type()
    print(html)


###################################################################
# Define function to test the password.
def check_password(user, passwd):

    conn = sqlite3.connect(DATABASE)
    c = conn.cursor()

    t = (user,)
    c.execute('SELECT * FROM users WHERE email=?', t)

    row = stored_password=c.fetchone()
    conn.close();

    if row != None: 
      stored_password=row[1]
      if (stored_password==passwd):
         return "passed"

    return "failed"


###################################################################
# Define function to test the verification
def check_verification(email):

    conn = sqlite3.connect(DATABASE)
    c = conn.cursor()

    t = (email,)
    c.execute('SELECT * FROM users WHERE email=?', t)

    row = info=c.fetchone()
    conn.close();
   
    if row != None:
        info=row[3]
        if (info==1):
            return "passed"

    return "failed"


##########################################################
# Try to register for an account.
def register(email, username, password):
    conn = sqlite3.connect(DATABASE)
    c = conn.cursor()

    user = (email, password, username, '0')
    c.execute('INSERT INTO users VALUES (?,?,?,?)', user)

    code = ''.join(random.SystemRandom().choice(string.uppercase + string.digits) for _ in xrange(10))
    verify = (email, code)
    c.execute('INSERT INTO verifications VALUES (?,?)', verify)

    conn.commit();
    conn.close();

    #Send the email for the user.
    me = "natfoyc@gmail.com"
    you = email

    msg = MIMEMultipart('alternative')
    msg['Subject'] = "MyLink Registration"
    msg['From'] = me
    msg['To'] = you

    text = """Hello {username},

    You've successfully initiated registration. To complete your registration please log in and input
    the following code:

    {code}

    You will then be required to log in again. Log in at data.cs.purdue.edu:7530/MyLink/login.cgi

    Thank you for registering,
    The MyLink Team
    """.format(username=username, code=code)

    htmll = """\
        <html>
        <head></head>
        <body>
            <p>Hello {username}, <br> <br>
            You've successfully initiated registration. To complete your registration please <br>
            log in and input the following code: <br> <br>
            {code} <br> <br>
            You will then be required to log in again. <br>
            <a href="data.cs.purdue.edu:7530/MyLink/login.cgi">Log in here</a> <br> <br>
            Thank you for registering, <br>
            The MyLink Team</p>
        </body>
        </html>
        """.format(username=username, code=code)

    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(htmll, 'html')

    msg.attach(part1)
    msg.attach(part2)

    mail = smtplib.SMTP('smtp.gmail.com', 587)

    mail.ehlo()

    mail.starttls()

    mail.login(me, 'Secret1234')
    mail.sendmail(me, you, msg.as_string())
    mail.quit()




##########################################################
# Diplay verification page
def display_verify(email):
    html="""
        <H1> This account has not verified its email address. </H1>
        <ul>
        <TABLE BORDER = 0>
        <FORM METHOD=post ACTION="login.cgi">
        <TR><TH>Verification code:</TH><TD><INPUT TYPE=text NAME="code"></TD><TR>
        <INPUT TYPE=hidden NAME="email" VALUE={email}>
        </TABLE>

        <INPUT TYPE=hidden NAME="action" VALUE="verify">
        <INPUT TYPE=submit VALUE="Enter">
        </FORM>
        </ul>
        """.format(email=email)

    print_html_content_type()
    print(html)


##########################################################
# Allows user to verify their account
def verify(email, code):
    conn = sqlite3.connect(DATABASE)
    c = conn.cursor()

    t = (email,)
    c.execute('SELECT * FROM verifications WHERE email = ?', t)

    row = info=c.fetchone()
   
    if row != None:
        info=row[1]
        if (info==code):
            c.execute('UPDATE users SET verified=\'1\' WHERE email = ?', t)
            c.execute('DELETE FROM verifications WHERE email = ?', t)
            conn.commit()
            conn.close()
            return "passed"

    conn.close()
    return "failed"


##########################################################
# Diplay the options of admin
def redirect(user, session):
    #print "Location: data.cs.purdue.edu:7530/MyLink/home.cgi\r\n"
    print "Status: 303 See other"
    print "Location: home.cgi?user={user}".format(user=user)


    print_html_content_type()
    print(html.format(user=user,session=session))

#################################################################
def create_new_session(user):
    return session.create_session(user)

##############################################################
def new_album(form):
    #Check session
    if session.check_session(form) != "passed":
       return

    html="""
        <H1> New Album</H1>
        """
    print_html_content_type()
    print(html);

##############################################################
def show_image(form):
    #Check session
    if session.check_session(form) != "passed":
       login_form()
       return

    # Your code should get the user album and picture and verify that the image belongs to this
    # user and this album before loading it

    #username=form["username"].value

    # Read image
    with open(IMAGEPATH+'/user1/test.jpg', 'rb') as content_file:
       content = content_file.read()

    # Send header and image content
    hdr = "Content-Type: image/jpeg\nContent-Length: %d\n\n" % len(content)
    print hdr+content

###############################################################################

def upload(form):
    if session.check_session(form) != "passed":
       login_form()
       return

    html="""
        <HTML>

        <FORM ACTION="login.cgi" METHOD="POST" enctype="multipart/form-data">
            <input type="hidden" name="user" value="{user}">
            <input type="hidden" name="session" value="{session}">
            <input type="hidden" name="action" value="upload-pic-data">
            <BR><I>Browse Picture:</I> <INPUT TYPE="FILE" NAME="file">
            <br>
            <input type="submit" value="Press"> to upload the picture!
            </form>
        </HTML>
    """

    user=form["user"].value
    s=form["session"].value
    print_html_content_type()
    print(html.format(user=user,session=s))

#######################################################

def upload_pic_data(form):
    #Check session is correct
    if (session.check_session(form) != "passed"):
        login_form()
        return

    #Get file info
    fileInfo = form['file']

    #Get user
    user=form["user"].value
    s=form["session"].value

    # Check if the file was uploaded
    if fileInfo.filename:
        # Remove directory path to extract name only
        fileName = os.path.basename(fileInfo.filename)
        open(IMAGEPATH+'/user1/test.jpg', 'wb').write(fileInfo.file.read())
        image_url="login.cgi?action=show_image&user={user}&session={session}".format(user=user,session=s)
        print_html_content_type()
	print ('<H2>The picture ' + fileName + ' was uploaded successfully</H2>')
        print('<image src="'+image_url+'">')
    else:
        message = 'No file was uploaded'

def print_html_content_type():
	# Required header that tells the browser how to render the HTML.
	print("Content-Type: text/html\n\n")

##############################################################
# Define main function.
def main():
    form = cgi.FieldStorage()
    if "action" in form:
        action=form["action"].value
        #print("action=",action)
        if action == "login":
            if "username" in form and "password" in form:
                #Test password
                username=form["username"].value
                password=form["password"].value
                if check_password(username, password)=="passed":
                    #Make sure user has verified email
                    if check_verification(username)=="passed":
                        session=create_new_session(username)
                        redirect(username, session)
                    else:
                        display_verify(username)

                else:
                   login_form()
                   print("<H3><font color=\"red\">Incorrect user/password</font></H3>")

        elif (action == "new-album"):
	       new_album(form)
        elif (action == "upload"):
          upload(form)
        elif (action == "show_image"):
          show_image(form)
        elif action == "upload-pic-data":
          upload_pic_data(form)

        elif action == "register":
            if "email" in form and "username" in form and "password" in form:
                email=form["email"].value
                username=form["username"].value
                password=form["password"].value
                register(email, username, password)
                login_form()
                print("<H3><font color = \"blue\">Registration input complete. Check your e-mail.</font></H3>")

        elif action == "verify":
            if "email" in form and "code" in form:
                email=form["email"].value
                code=form["code"].value
                #Test if the code is right
                if verify(email, code)=="passed":
                    login_form()
                    print("<H3><font color = \"blue\">Verification code correct. Your account is verified."
                        +"Please log in.</font></H3>")
                else:
                    display_verify(email)
                    print("<H3><font color =\"red\"> Verification code failed.</font></H3>")

        else:
            login_form()
    else:
        login_form()

###############################################################
# Call main function.
main()
