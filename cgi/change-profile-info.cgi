#!/usr/bin/python

import cgi, string, sys, os, re, random
import cgitb; cgitb.enable()  # for troubleshooting
import sqlite3
import session

form = cgi.FieldStorage()

user_email = form.getvalue('user')

def print_html_content_type():
	# Required header that tells the browser how to render the HTML.
	print("Content-Type: text/html\n\n")

# Put your relevant functions and imports

def welcome():
	html="""
	<html>

<head>
	<title>Change Profile Info</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
</head>

<body>
	<div class="main">
		<h1>Change Profile Info</h1>
		<form class="change-profile-info" action="change-password.cgi">
			<div class="label">Name:</div>
			<input type="text" class="name" />
			<div class="label">Birthday:</div>
			<input type="text" class="birthday" />
			<div class="label">Address:</div>
			<input type="text" class="address" />
			<div class="label">Birthplace:</div>
			<input type="text" class="birthplace" />
			<br />
			<input type="submit" />
		</form>
	</div>
</body>

</html>
	""".format(name=user_email)

	print_html_content_type()
	print(html)
	
welcome()