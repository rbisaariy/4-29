#!/usr/bin/python

import cgi, string, sys, os, re, random
import cgitb; cgitb.enable()  # for troubleshooting
import sqlite3
import session

form = cgi.FieldStorage()

user_email = form.getvalue('user')

def print_html_content_type():
	# Required header that tells the browser how to render the HTML.
	print("Content-Type: text/html\n\n")

# Put your relevant functions and imports

def welcome():
	html="""
	<html>

<head>
	<title>Change Password</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
</head>

<body>
	<div class="main">
		<h1>Change Password</h1>
		<form class="change-password" action="change-password.cgi">
			<input type="password" placeholder="Old Password" class="old-password" />
			<input type="password" placeholder="New Password" class="new-password" />
			<input type="password" placeholder="Confirm Password" class="password-confirm" />
			<input type="submit" />
		</form>
	</div>
</body>

</html>
	""".format(name=user_email)

	print_html_content_type()
	print(html)
	
welcome()