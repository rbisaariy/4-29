#!/usr/bin/python

import cgi, string, sys, os, re, random
import cgitb; cgitb.enable()  # for troubleshooting
import sqlite3
import session

form = cgi.FieldStorage()

user_email = form.getvalue('user')

def print_html_content_type():
	# Required header that tells the browser how to render the HTML.
	print("Content-Type: text/html\n\n")

def welcome():
	html2="""
<html>

<head>
	<title>Hello Rohan!</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
</head>

<body>
	<div class="main">
		<h1>Rohan's Profile</h1>
		<div class="profile">
			<div class="profile-pic">
				<img src="/images/user1/test.jpg" />
				<button type="button" name="change-pic">Change your profile picture</button>
			</div>
			<div class="profile-info">
				<div class="name">
					<div class="label">Name:</div>
					<div class="value">Rohan Bisariya</div>
				</div>
				<div class="birthday">
					<div class="label">Birthday:</div>
					<div class="value">August 3, 1996</div>
				</div>
				<div class="address">
					<div class="label">Address:</div>
					<div class="value">314 N. Russell St.</div>
				</div>
				<div class="birthplace">
					<div class="label">Birthplace:</div>
					<div class="value">Indianapolis, IN</div>
				</div>
				<div class="profile-edit-container">
					<a class="profile-info" href="change-profile-info.cgi">Change profile info</a>
					<a class="pwd-change" href="change-password.cgi">Change password</a>
					<a class="friends" href="your-friends-page.html">Friends</a>
				</div>
			</div>
		</div>
		<hr />
		<div class="posts">
			<h1>Write a New Post</h1>
			<form class="new-post-container" action="newpost.cgi">
				<div class="post-title">
					<input type="text" placeholder="Title" />
				</div>
				<div class="post-content">
					<input type="text" placeholder="Post" />
				</div>
				<select>
					<option value="circle-id-0">Circle Name</option>
					<option value="circle-id-1">Another Circle Name</option>
				</select>
				<button type="button" name="post-upload-photo">Upload Photo</button>
				<input type="submit" name="send-post" />
			</form>
			<hr />
			<h1>Rohan's Posts</h1>
			<div class="post">
				<div class="post-img">
					<img src="/images/user1/image-101.jpg" />
				</div>
				<div class="post-info">
					<div class="post-title">Rohan's Most Recent Post</div>
					<div class="post-content">This is the posts content.</div>
					<div class="post-date">April 15, 2015</div>
					<div class="post-circle">All friends</div>
				</div>
			</div>
			<div class="post">
				<div class="post-img">
					<img src="/images/user1/image-102.jpg" />
				</div>
				<div class="post-info">
					<div class="post-title">An Older Post</div>
					<div class="post-content">This is a longer post content. Bacon ipsum dolor amet t-bone filet mignon leberkas shankle cupim meatloaf alcatra kevin ribeye shoulder chicken tenderloin. Drumstick pork loin sirloin meatball tongue prosciutto kevin spare ribs, strip steak beef pork ribeye leberkas swine jowl. Beef meatloaf pastrami pork chop. Ham ribeye strip steak beef. Turkey spare ribs bresaola venison chuck meatball pork chop porchetta flank prosciutto pastrami tenderloin turducken jerky. Doner filet mignon meatball shankle capicola jowl chuck.</div>
					<div class="post-date">August 12, 2014</div>
					<div class="post-circle">Family</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
	""".format(name=user_email)

	print_html_content_type()
	print(html2)

welcome()
