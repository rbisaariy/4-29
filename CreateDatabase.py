#!/usr/bin/python

import sqlite3
conn = sqlite3.connect('mylink.db')

c = conn.cursor()

# Turn on foreign key support
c.execute("PRAGMA foreign_keys = ON")

# Create users table
c.execute('''CREATE TABLE users
	     (email VARCHAR(30) NOT NULL UNIQUE, 
	      password VARCHAR(30) NOT NULL,
	      username VARCHAR(30) NOT NULL,
	      verified INT(1) NOT NULL,
	      PRIMARY KEY(email))''')

# Create circles table
c.execute('''CREATE TABLE circles
		 (circle_id INT UNSIGNED NOT NULL PRIMARY KEY,
		  owner VARCHAR(30) NOT NULL,
		  circle_name VARCHAR(30) NOT NULL,
		  FOREIGN KEY (owner) REFERENCES users(email)
		  	ON UPDATE CASCADE
		  	ON DELETE CASCADE,
		  UNIQUE(owner, circle_name))''')

#Create a table that stores users to circles
c.execute('''CREATE TABLE circle_values
		  (circle_owner INT UNSIGNED NOT NULL,
		   user VARCHAR(30) NOT NULL,
		   FOREIGN KEY (circle_owner) REFERENCES circles(circle_id)
		   	ON UPDATE CASCADE
		   	ON DELETE CASCADE,
		   FOREIGN KEY(user) REFERENCES users(email)
		   	ON UPDATE CASCADE
		   	ON DELETE CASCADE,
		   PRIMARY KEY(circle_owner, user))''')


# Create album table
# Visibility is 'public' or 'private'
c.execute('''CREATE TABLE albums
	     (name VARCHAR(30) NOT NULL,
	      owner VARCHAR(30) NOT NULL,
	      visibility VARCHAR(30) NOT NULL,
	      FOREIGN KEY (owner) REFERENCES users(email)
	      	ON UPDATE CASCADE
	      	ON DELETE CASCADE,
	      PRIMARY KEY(name, owner))''')

# Create pictures table
c.execute('''CREATE TABLE pictures
	     (path VARCHAR(260) NOT NULL,
	      album VARCHAR(30) NOT NULL,
	      owner VARCHAR(30) NOT NULL,
	      FOREIGN KEY(album, owner) REFERENCES albums(name, owner)
	      	ON UPDATE CASCADE
	      	ON DELETE CASCADE,
	      FOREIGN KEY(owner) REFERENCES users(email)
	      	ON UPDATE CASCADE
	      	ON DELETE CASCADE,
	      PRIMARY KEY(path))''')

# Create sessions table
c.execute('''CREATE TABLE sessions
	     (user VARCHAR(30) NOT NULL,
	      session TEXT NOT NULL,
	      FOREIGN KEY(user) REFERENCES users(email)
	      	ON UPDATE CASCADE
	      	ON DELETE CASCADE,
	      PRIMARY KEY(session))''')

# Create verifications table
c.execute('''CREATE TABLE verifications
		 (email VARCHAR(30) NOT NULL,
		  verify VARCHAR(30) NOT NULL,
		  PRIMARY KEY(email))''')


# Save the changes
conn.commit()

# Close the connection
conn.close()
